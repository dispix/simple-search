import * as React from "react"

import Fallback from "./Fallback"
import HomePage, { IProps } from "./HomePage"

function SuspendedHomePage(props: IProps) {
  return (
    <React.Suspense fallback={Fallback}>
      <HomePage {...props} />
    </React.Suspense>
  )
}

export default SuspendedHomePage
