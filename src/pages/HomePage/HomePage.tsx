import Typography from "@material-ui/core/Typography"
import * as React from "react"
import { RouteComponentProps } from "react-router"

import PagesNavigation from "../../components/PagesNavigation/PagesNavigation"
import PostList from "../../features/PostList"

export type IProps = RouteComponentProps

const pageRegExp = /(page=)([0-9]*)/
const getPage = (str: string) => {
  const result = pageRegExp.exec(str)

  return result ? Number(result[2]) : 1
}
const setPage = (str: string, page: number) =>
  str.replace(pageRegExp, (_, base) => `${base}${page}`)

function HomePage({ history, location: { search } }: IProps) {
  const currentPage = getPage(search)

  if (!search || currentPage < 1) {
    history.push({ search: "?page=1" })
  }

  const useNavCallback = (fn: () => void) =>
    React.useCallback(fn, [currentPage])
  const handlePrevious = useNavCallback(() =>
    history.push({
      search: setPage(search, Math.max(currentPage - 1, 1)),
    }),
  )
  const handleNext = useNavCallback(() =>
    history.push({
      search: setPage(search, currentPage + 1),
    }),
  )

  return (
    <React.Fragment>
      <Typography variant="h2">Posts</Typography>
      <PagesNavigation
        onPrevious={currentPage > 1 ? handlePrevious : null}
        onNext={handleNext}
      />
      <React.Suspense fallback={null}>
        <PostList page={currentPage} />
      </React.Suspense>
    </React.Fragment>
  )
}

export default HomePage
