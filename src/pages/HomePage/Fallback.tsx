import Card from "@material-ui/core/Card"
import CardHeader from "@material-ui/core/CardHeader"
import * as React from "react"

function Fallback() {
  return Array(10)
    .fill(null)
    .map((_, i) => (
      <Card key={i} style={{ height: 200 }}>
        <CardHeader title="..." />
      </Card>
    ))
}

export default Fallback
