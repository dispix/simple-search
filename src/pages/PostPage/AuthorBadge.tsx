import Typography from "@material-ui/core/Typography"
import * as React from "react"
import { unstable_createResource } from "react-cache"

import UserBadge from "../../components/UserBadge"
import Api from "../../services/api"

interface IProps {
  userId: string
}

const userResource = unstable_createResource(Api.getUser)

function AuthorBadge({ userId }: IProps) {
  const user = userResource.read(userId)

  return user && <UserBadge name={user.name} />
}

AuthorBadge.fallback = <UserBadge name="..." />

export default AuthorBadge
