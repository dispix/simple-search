import { Button, Theme } from "@material-ui/core"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/styles"
import * as React from "react"
import { unstable_createResource } from "react-cache"
import { Redirect, RouteComponentProps } from "react-router"
import { Link } from "react-router-dom"

import Api from "../../services/api/Api"

import AuthorBadge from "./AuthorBadge"

type IProps = RouteComponentProps<{ id: string }>

const useStyle = makeStyles((theme: Theme) => ({
  article: {
    display: "grid",
    gridGap: `${theme.spacing.unit * 2}px`,
  },
}))

const postResource = unstable_createResource(Api.getPost)

function PostPage(props: IProps) {
  const post = postResource.read(props.match.params.id)
  const classes = useStyle("")

  return post ? (
    <article className={classes.article}>
      <Typography noWrap={true} variant="h1">
        {post.title}
      </Typography>
      <React.Suspense fallback={AuthorBadge.fallback}>
        <AuthorBadge userId={post.userId} />
      </React.Suspense>
      <Typography paragraph={true} component="section">
        {post.body}
      </Typography>
      <Link to="/">
        <Button>Home</Button>
      </Link>
    </article>
  ) : (
    <Redirect to="/" />
  )
}

export default PostPage
