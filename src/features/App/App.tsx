import { createMuiTheme } from "@material-ui/core/styles"
import { ThemeProvider } from "@material-ui/styles"
import * as React from "react"
import { Redirect, Route, Switch } from "react-router"

import Main from "../../components/Main"
import Router from "../Router"

import "./App.css"

const HomePage = React.lazy(() => import("../../pages/HomePage"))
const PostPage = React.lazy(() => import("../../pages/PostPage"))

const HomeRedirect = () => <Redirect to="/" />
const theme = createMuiTheme()

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <React.Suspense fallback={null}>
          <Main>
            <Switch>
              <Route path="/posts/:id" component={PostPage} />
              <Route exact={true} path="/" component={HomePage} />
              <Route component={HomeRedirect} />
            </Switch>
          </Main>
        </React.Suspense>
      </Router>
    </ThemeProvider>
  )
}

export default App
