import * as React from "react"
import { BrowserRouter } from "react-router-dom"

export interface IProps {
  children: React.ReactChild
}

function Router(props: IProps) {
  return <BrowserRouter>{props.children}</BrowserRouter>
}

export default Router
