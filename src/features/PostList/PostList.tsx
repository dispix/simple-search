import Card from "@material-ui/core/Card"
import CardContent from "@material-ui/core/CardContent"
import CardHeader from "@material-ui/core/CardHeader"
import List from "@material-ui/core/List"
import Typography from "@material-ui/core/Typography"
import * as React from "react"
import { Link } from "react-router-dom"

import postsResource from "./postsResource"
import useStyles from "./useStyles"

interface IProps {
  page: number
  limit: number
}

function PostList({ limit, page }: IProps) {
  const classes = useStyles({})
  const posts = postsResource.read({ page, limit })
  postsResource.preload({ page: page + 1, limit })

  return (
    posts && (
      <List>
        {posts.map(post => (
          <Link key={post.id} to={`posts/${post.id}`}>
            <Card className={classes.card}>
              <CardHeader title={post.title} />
              <CardContent>
                <Typography>{post.body}</Typography>
              </CardContent>
            </Card>
          </Link>
        ))}
      </List>
    )
  )
}

PostList.defaultProps = {
  limit: 10,
  page: 1,
}

export default PostList
