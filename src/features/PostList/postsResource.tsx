import { unstable_createResource } from "react-cache"

import Api from "../../services/api"

const postsResource = unstable_createResource(
  Api.getPosts,
  ({ page, limit }) => `${page}.${limit}`,
)

export default postsResource
