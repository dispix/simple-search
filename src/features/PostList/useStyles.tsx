import makeStyles from "@material-ui/styles/makeStyles"

const useStyles = makeStyles(theme => ({
  card: {
    margin: theme.spacing.unit,
  },
}))

export default useStyles
