import Omit from "../../utils/Omit"

import { API_URL } from "./constants"

export function get(
  url: string,
  init?: Omit<RequestInit, "method">,
): Promise<Response> {
  return fetch(API_URL + url, { ...init, method: "GET" })
}
