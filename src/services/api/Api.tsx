import { get } from "./request"
import { IPost, IUser } from "./types"

class Api {
  public static async getPosts({
    page = 1,
    limit = 10,
  }: {
    page?: number
    limit?: number
  }): Promise<IPost[] | null> {
    const path = `/posts?_page=${page}&_limit=${limit}`

    return Api.handleCall(get(path))
  }

  public static async getPost(id: string): Promise<IPost | null> {
    return Api.handleCall(get(`/posts/${id}`))
  }

  public static async getUser(id: string): Promise<IUser | null> {
    return Api.handleCall(get(`/users/${id}`))
  }

  private static async handleCall<T>(
    responsePromise: Promise<Response>,
  ): Promise<T | null> {
    try {
      const response = await responsePromise

      return response.ok ? await response.json() : null
    } catch (e) {
      return null
    }
  }
}

export default Api
