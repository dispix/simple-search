import { makeStyles } from "@material-ui/styles"
import * as React from "react"

interface IProps {
  children: React.ReactChild
}

const useStyles = makeStyles({
  main: {
    margin: "auto",
    maxWidth: 900,
  },
})

function Main(props: IProps) {
  const classes = useStyles({})

  return <main className={classes.main}>{props.children}</main>
}

export default React.memo(Main)
