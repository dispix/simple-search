import Avatar from "@material-ui/core/Avatar"
import Chip from "@material-ui/core/Chip"
import PersonIcon from "@material-ui/icons/Person"
import { makeStyles } from "@material-ui/styles"
import * as React from "react"

interface IProps {
  name: string
}

const useStyle = makeStyles({
  chip: {
    width: "fit-content",
  },
})

function UserBadge({ name }: IProps) {
  const classes = useStyle("")

  return (
    <Chip
      className={classes.chip}
      variant="outlined"
      avatar={
        <Avatar component="span">
          <PersonIcon />
        </Avatar>
      }
      label={name}
    />
  )
}

UserBadge.defaultProps = {
  name: "",
}

export default React.memo(UserBadge)
