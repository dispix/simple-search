import Button from "@material-ui/core/Button"
import Icon from "@material-ui/core/Icon"
import NavigateBefore from "@material-ui/icons/NavigateBefore"
import NavigateNext from "@material-ui/icons/NavigateNext"
import * as React from "react"

interface IProps {
  onPrevious: (() => void) | null
  onNext: (() => void) | null
}

function PagesNavigation(props: IProps) {
  return (
    <section>
      <Button
        disabled={!props.onPrevious}
        onClick={props.onPrevious || undefined}
      >
        <Icon>
          <NavigateBefore />
        </Icon>
        Previous
      </Button>
      <Button disabled={!props.onNext} onClick={props.onNext || undefined}>
        Next
        <Icon>
          <NavigateNext />
        </Icon>
      </Button>
    </section>
  )
}

export default React.memo(PagesNavigation)
